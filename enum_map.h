#pragma once
#include <cstddef>
#include <initializer_list>
#include <iterator>
#include <optional>
#include <type_traits>
#include <utility>
namespace jm {

template <
    typename E, typename T,
    std::size_t N = static_cast<std::size_t>(E::MAX)
>
class enum_map {
private:
    std::optional<T> data[N];
    template <typename U>
    class base_iterator: public std::iterator<std::forward_iterator_tag, U> {
    private:
        typename std::conditional<
            std::is_const<U>::value,
            const std::optional<typename std::remove_const<U>::type>,
            std::optional<U>
        >::type* data;
        std::size_t i;
    public:
        constexpr base_iterator() = default;
        constexpr base_iterator(decltype(data) data, decltype(i) i):
            data(data), i(i) {}
        base_iterator(const base_iterator<U>&) = default;
        bool operator==(const base_iterator<U>& other) const {
            return data == other.data && i == other.i;
        }
        bool operator!=(const base_iterator<U>& other) const {
            return data != other.data || i != other.i;
        }
        U& operator*() const {
            return *(data[i]);
        }
        U* operator->() const {
            return &*(data[i]);
        }
        base_iterator<U>& operator++() {
            do { ++i; } while (i < N && !data[i].has_value());
            return *this;
        }
        base_iterator<U> operator++(int) {
            base_iterator<U> it = *this;
            do { ++i; } while (i < N && !data[i].has_value());
            return it;
        }
    };
public:
    using iterator = base_iterator<T>;
    using const_iterator = base_iterator<const T>;
    constexpr enum_map() = default;
    constexpr enum_map(std::initializer_list<std::pair<E, T>> il) {
        for (const auto& p: il) {
            data[static_cast<std::size_t>(p.first)].emplace(p.second);
        }
    }
    T& operator[](E e) {
        if (!data[static_cast<std::size_t>(e)].has_value()) {
            data[static_cast<std::size_t>(e)].emplace();
        }
        return *(data[static_cast<std::size_t>(e)]);
    }
    const T& operator[](E e) const {
        return *(data[static_cast<std::size_t>(e)]);
    }
    bool count(E e) const {
        return data[static_cast<std::size_t>(e)].has_value();
    }
    void insert(E e, const T& t) {
        data[static_cast<std::size_t>(e)].emplace(t);
    }
    void erase(E e) {
        data[static_cast<std::size_t>(e)].reset();
    }
    iterator begin() {
        return ++iterator(data, std::size_t(-1));
    }
    const_iterator begin() const {
        return ++const_iterator(data, std::size_t(-1));
    }
    iterator end() {
        return {data, N};
    }
    const_iterator end() const {
        return {data, N};
    }
};

}
